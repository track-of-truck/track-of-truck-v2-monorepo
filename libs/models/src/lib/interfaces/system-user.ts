import { Base } from './base';

export interface SystemUser extends Base {
  systemUserId: string;
  name: string;
  email: string;
}
