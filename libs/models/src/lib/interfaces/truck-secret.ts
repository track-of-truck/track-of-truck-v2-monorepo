import { Base } from './base';

export interface TruckSecret extends Base{
  truckSecretId: string;
  truckId: string;
  secret: string;
}