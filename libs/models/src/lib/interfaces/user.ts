import { Base } from './base';
import { GeoPoint } from './geo-point';

export interface User extends Base {
  userId: string;
  location?: GeoPoint;
  language?: string;
  notifications: boolean;
}
