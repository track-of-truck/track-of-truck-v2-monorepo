import { Base } from './base';
import { GeoPoint } from './geo-point';

export interface Truck extends Base {
  truckId: string;

  registrationNumber: string;
  trailerRegistrationNumber?: string;
  tags: string[]; // custom tags added by the management to identify vehicles
  
  lastKnownLocation?: GeoPoint;
  lastKnownLocationGeoHash?: string;
  lastUpdatedTime?: Date;
}