import * as firebase from 'firebase/app';
import { firestore } from 'firebase-admin';
import 'firebase/firestore';

export type FirestoreRecord<T> = Omit<Omit<T, 'createdAt'>, 'updatedAt'> & {
  createdAt: firebase.firestore.Timestamp | firestore.Timestamp;
  updatedAt: firebase.firestore.Timestamp | firestore.Timestamp;
};
