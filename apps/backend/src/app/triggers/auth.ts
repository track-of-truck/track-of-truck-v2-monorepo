import * as functions from 'firebase-functions';
import { Logger } from '../utils/logger';

// In the scenario where user record is created through the local firebase auth client,
// the firestore record has to be created by this trigger
export const onNewUser = functions.auth.user().onCreate(async (user) => {
  Logger.info('New user created on auth', user.toJSON());
});
