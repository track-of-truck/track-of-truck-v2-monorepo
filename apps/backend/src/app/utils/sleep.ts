export async function sleep(time: number) {
  return new Promise<boolean>((resolve) => {
    setTimeout(() => resolve(true), time);
  });
}
