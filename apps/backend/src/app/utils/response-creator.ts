import { ServerResponse } from '@track-of-truck-monorepo/models';

export function makeResponse<T = any>(
  success: boolean,
  message = 'success',
  statusCode = 200,
  data?: T
): ServerResponse {
  return { success, message, data, statusCode };
}
