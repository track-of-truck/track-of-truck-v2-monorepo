import * as admin from 'firebase-admin';
import { SystemUser } from '@track-of-truck-monorepo/models';
import { Logger } from '../utils/logger';

export class SystemUserService {
  async createSystemUser(email: string, name: string, password: string) {
    const auth = admin.auth();
    const db = admin.firestore();

    Logger.info('Creating system user', { email, name });
    const userRecord = await auth.createUser({
      email,
      password,
      displayName: name,
    });

    await auth.setCustomUserClaims(userRecord.uid, {
      isAdmin: true,
    });

    const systemUser: Partial<SystemUser> = {
      systemUserId: userRecord.uid,
      email,
      name,
    };
    const ref = db.collection('SystemUsers').doc(systemUser.systemUserId);
    await ref.set({
      ...systemUser,
      createdAt: admin.firestore.FieldValue.serverTimestamp(),
      updatedAt: admin.firestore.FieldValue.serverTimestamp(),
    });
    Logger.info('Created system user', {
      email,
      userId: systemUser.systemUserId,
    });
  }

  async removeSystemUser(userId: string) {
    const auth = admin.auth();
    const db = admin.firestore();

    const ref = db.collection('SystemUsers').doc(userId);
    Logger.info('Deleting system user', userId);
    await auth.deleteUser(userId);
    await ref.delete();
    Logger.info('Deleted system user', userId);
  }
}
