import * as admin from 'firebase-admin';
import * as qrcode from 'qrcode';
import {
  FirestoreOperation,
  FirestoreRecord,
  GeoPoint,
  Truck,
  TruckSecret,
} from '@track-of-truck-monorepo/models';
import { Writable } from 'stream';
import { Logger } from '../utils/logger';

export class TruckService {
  async generateSecret(truckId: string) {
    const db = admin.firestore();

    const rawSecret = db.collection('Trucks').doc().id;
    const buffer = new Buffer(rawSecret);
    const secret = buffer.toString('base64');

    const ref = db
      .collection('Trucks')
      .doc(truckId)
      .collection('Secrets')
      .doc('secret');
    const truckSecret: Partial<TruckSecret> = {
      truckSecretId: 'secret',
      truckId,
      secret,
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    await ref.set({
      ...truckSecret,
      createdAt: admin.firestore.FieldValue.serverTimestamp(),
      updatedAt: admin.firestore.FieldValue.serverTimestamp(),
    });

    return truckSecret;
  }

  async generateSecretQRCode(truckId: string, stream: Writable) {
    const db = admin.firestore();

    const ref = db
      .collection('Trucks')
      .doc(truckId)
      .collection('Secrets')
      .doc('secret');
    const truckSecretSnapshot = await ref.get();
    if (!truckSecretSnapshot.exists) {
      throw new Error('Record does not exist!');
    }

    const truckSecret = truckSecretSnapshot.data() as FirestoreRecord<
      TruckSecret
    >;
    const data = JSON.stringify({ truckId, secret: truckSecret.secret });

    Logger.info('Streaming the QR code');
    qrcode.toFileStream(stream, data);
  }

  async recordLocationOfTruck(truckId: string, location: GeoPoint) {
    const db = admin.firestore();
    const ref = db.collection('Trucks').doc(truckId);
    const update: Partial<FirestoreOperation<Truck>> = {
      lastKnownLocation: location,
      lastKnownLocationGeoHash: 'NOT CALCULATED',
      updatedAt: admin.firestore.FieldValue.serverTimestamp(),
    };
    Logger.info('Updating the location of truck');
    await ref.update({
      ...update,
      lastUpdatedTime: admin.firestore.FieldValue.serverTimestamp(),
    });
    Logger.info('Updated the location of the truck');
  }

  async validateTruckSecret(truckId: string, secret: string) {
    const db = admin.firestore();
    const ref = db
      .collection('Trucks')
      .doc(truckId)
      .collection('Secrets')
      .doc('secret');
    const snapshot = await ref.get();
    if (!snapshot.exists) {
      throw new Error('Record does not exist!');
    }

    const truckSecret = snapshot.data() as FirestoreRecord<TruckSecret>;
    if (truckSecret.secret !== secret) {
      throw new Error('Invalid secret');
    }

    return true;
  }
}
