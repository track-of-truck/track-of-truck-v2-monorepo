import * as express from 'express';
import { ServerError } from '../models/server-error';
import { NotificationService } from '../services/notification.service';
import { TruckService } from '../services/truck.service';
import { Logger } from '../utils/logger';
import { makeResponse } from '../utils/response-creator';

export const truckRouter = express.Router();

const truckService = new TruckService();
const notificationService = new NotificationService();

truckRouter.get('/:truckId/secret', async (req, res) => {
  try {
    const { truckId } = req.params;
    if (!truckId) {
      throw new ServerError('Malformed input', 400);
    }

    res.setHeader('content-type', 'image/png');
    await truckService.generateSecretQRCode(truckId as string, res);
  } catch (error) {
    Logger.error('Could not stream the QR code', error.message);
    res.setHeader('content-type', 'application/json');
    res.send(makeResponse(false, error.message, error.code));
  }
});

truckRouter.put('/:truckId/location', async (req, res) => {
  try {
    const { truckId } = req.params;
    const { location, secret } = req.body;
    if (!location || !secret || !truckId) {
      throw new ServerError('Malformed Input', 400);
    }

    // TODO: This is a weak authentication mechanism. Update to a JWT based method with a token timeout

    // validating the input
    await truckService.validateTruckSecret(truckId, secret);

    // update the locatian
    await truckService.recordLocationOfTruck(truckId, location);

    // notify users
    await notificationService.notifyNearbyUsers(truckId, location);

    res.send(makeResponse(true, 'Updated the location of the truck'));
  } catch (error) {
    Logger.error('Could not update location of truck', error.message);
    res.send(makeResponse(false, error.message, error.code));
  }
});
