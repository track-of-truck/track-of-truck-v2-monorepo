import * as express from 'express';
import { json } from 'body-parser';
import { makeResponse } from '../utils/response-creator';
import { systemUserRouter } from './system-user';
import { truckRouter } from './truck';

export const apiRouter = express.Router();

apiRouter.use(json());

apiRouter.use('/system-user', systemUserRouter);
apiRouter.use('/truck', truckRouter);

apiRouter.get('/', async (req, res) => {
  res.send(makeResponse(true, 'Hello there.', 200, req.headers));
});
