import * as express from 'express';
import { ServerError } from '../models/server-error';
import { SystemUserService } from '../services/system-user.service';
import { Logger } from '../utils/logger';
import { makeResponse } from '../utils/response-creator';

export const systemUserRouter = express.Router();

const systemUserService = new SystemUserService();
// TODO: create an auth middleware to authorize the routes

systemUserRouter.post('/', async (req, res) => {
  try {
    const { email, password, name } = req.body;
    if (!email || !password || !name) {
      throw new ServerError('Malformed input', 400);
    }

    await systemUserService.createSystemUser(email, name, password);
    res.send(makeResponse(true, 'Created new System User', 201));
  } catch (error) {
    Logger.error('Colud not create new system user', error.message);
    res.send(makeResponse(false, error.message, error.code));
  }
});

systemUserRouter.delete('/', async (req, res) => {
  try {
    const { userId } = req.body;
    if (!userId) {
      throw new ServerError('Malformed input', 400);
    }

    await systemUserService.removeSystemUser(userId);
    res.send(makeResponse(true, 'Deleted System User', 200));
  } catch (error) {
    Logger.error('Colud not delete system user', error.message);
    res.send(makeResponse(false, error.message, error.code));
  }
});
