import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';
import { User, SystemUser } from '@track-of-truck-monorepo/models';
import { apiRouter } from './app/routes';
import { Logger } from './app/utils/logger';

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    export interface Request {
      user: User | SystemUser;
    }
  }
}

admin.initializeApp();

const app = express();

app.set('view engine', 'jade');

app.use((req, _, next) => {
  Logger.debug('HTTP Request', { method: req.method, path: req.path });
  next();
});

app.use('/api', apiRouter);

export const api = functions.https.onRequest(app);
export * from './app/triggers';
